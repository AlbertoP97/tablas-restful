const express = require('express');

function suma(req, res, next){
    const n1 = parseInt(req.params.n1);
    const n2 = parseInt(req.params.n2);
    res.send(`El resultado de la suma de ${n1} + ${n2} es: ${n1+n2}`);
}

function resta(req, res, next){
    const n1 = parseInt(req.params.n1);
    const n2 = parseInt(req.params.n2);
    res.send(`El resultado de la resta de ${n1} - ${n2} es: ${n1-n2}`);
}

function multiplicacion(req, res, next){
    const n1 = parseInt(req.params.n1);
    const n2 = parseInt(req.params.n2);
    res.send(`El resultado de la multiplicacion de ${n1} + ${n2} es: ${n1*n2}`);
}

function division(req, res, next){
    const n1 = parseInt(req.params.n1);
    const n2 = parseInt(req.params.n2);
    res.send(`El resultado de la division de ${n1} / ${n2} es: ${n1/n2}`);
}

function potencia(req, res, next){
    const n1 = parseInt(req.params.n1);
    const n2 = parseInt(req.params.n2);
    res.send(`El resultado de la potencia de ${n1} ^ ${n2} es: ${n1^n2}`);
}


module.exports = {
    suma,resta,multiplicacion,division,potencia
}